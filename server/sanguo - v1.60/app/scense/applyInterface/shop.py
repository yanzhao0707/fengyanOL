#coding:utf8
'''
Created on 2011-4-13

@author: sean_lan
'''
from app.scense.core.PlayersManager import PlayersManager
from app.scense.netInterface.pushObjectNetInterface import pushOtherMessage
from app.scense.core.shop.PublicShop import PublicShop
from app.scense.core.shop.shopmanager import ShopManager
from app.scense.core.language.Language import Lg


def getShopInfo(dynamicId,characterId,shopCategory):
    '''获取商店信息
    @param dynamicId: int 客户端的id
    @param characterId: int 角色的id
    @param shopType: int 商店类型  1武器 2防具 3材料 4 杂货
    '''
    player = PlayersManager().getPlayerByID(characterId)
    if not player or not player.CheckClient(dynamicId):
        return {'result':False,'message':Lg().g(18)}
    package = player.shop.getShopPackage(shopCategory)
    result = package.getPackageItemInfo()
    refreshShopTime = player.shop.getRefreshShopTime()
    data = {'shopCategory':shopCategory,'refreshShopTime':refreshShopTime,'packageItemInfo':result}
    return {'result':True,'data':data}

def buyItemInMyshop(dynamicId,characterId,itemTemplateId):
    '''获取商店信息
    @param dynamicId: int 客户端的id
    @param characterId: int 角色的id
    @param itemTemplateId: int 物品的模板Id
    '''
    player = PlayersManager().getPlayerByID(characterId)
    if not player or not player.CheckClient(dynamicId):
        return {'result':False,'message':Lg().g(18)}
    data  = player.shop.buyItemInMyshop(itemTemplateId)
    return data
    
def getMallItemInfo(dynamicId,characterId,page,tag):
    '''获取商城信息
    @param dynamicId: int 客户端的id
    @param characterId: int 角色的id
    @param page: int 页面号
    '''
    pushOtherMessage(905, u'该功能暂未开放,敬请期待V1.7版本', [dynamicId])

def buyItemInMall(dynamicId,characterId,itemTemplateId,count,priceType,buyType,presentName):
    '''购买商城中的物品
    @param dynamicId: int 客户端的id
    @param characterId: int 角色的id
    @param itemTemplateId: 物品的模板id
    @param priceType: int 1为钻价格 2为绑定钻价格
    @param butType: int 购买类型 0购买1赠送
    @param presentName: string 赠送角色的名称
    '''
    pushOtherMessage(905, u'该功能暂未开放,敬请期待V1.7版本', [dynamicId])


            


def getNpcShopInfo(dynamicId,characterId,npcId,shopCategory,curPage):
    '''获取公共商店信息'''
    player = PlayersManager().getPlayerByID(characterId)
    if not player or not player.CheckClient(dynamicId):
        return {'result':False,'message':Lg().g(18)}
    publicshop = ShopManager().getShopByID(npcId)
    if not publicshop:
        publicshop = PublicShop(npcId)
        ShopManager().addShop(publicshop)
    if shopCategory ==0:
        data = publicshop.getPublicShopInfo(curPage)
    else:
        data = publicshop.getRepurchaseInfo(characterId)
    return {'result':True,'data':data}
        
def NpcShopSellItem(dynamicId,characterId,itemPos,packageType,curpage,stack):
    '''出售物品
    @param itemPos: int 
    @param packageType: int 背包标签页 1道具 2任务物品
    @param curpage: int 当前页数
    @param stack: int 出售数量
    '''
    player = PlayersManager().getPlayerByID(characterId)
    if not player or not player.CheckClient(dynamicId):
        return {'result':False,'message':Lg().g(18)}
    data = player.pack.sellItemInpack(itemPos,packageType,curpage,stack)
    pushOtherMessage(905, data.get('message',''), [dynamicId])
    return data

def NpcShopBuyItem(dynamicId,characterId,itemId,opeType,buyNum,npcId):
    '''购买，回购商品
    @param opeType: int 操作类型 0购买1购回
    @param buyNum: int 购买的数量
    '''
    player = PlayersManager().getPlayerByID(characterId)
    if not player or not player.CheckClient(dynamicId):
        return {'result':False,'message':Lg().g(18)}
    if opeType==0:
        data  = player.shop.buyItemInMyshop(itemId,buyNum,npcId)
    else:
        data = player.shop.RepurchaseItem(itemId)
    if data.get('result',False):
        if opeType == 0:
            pushOtherMessage(905, Lg().g(193), [dynamicId])
        else:
            pushOtherMessage(905, Lg().g(195), [dynamicId])
    return data


